# Conversion-Service Mono-Repository

This Mono-Repo setup includes:

- lerna managed packages for:
  - [conversion-service](packages/conversion-service/README.md)
  - [auto-scaler](packages/auto-scaler/README.md)
  - [redis-service](packages/redis/README.md) (Message Queue & load balancing)
  - [logger](packages/logger/README.md)

For further information checkout the README files of the corresponding `package`.
