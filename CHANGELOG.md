# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0-5](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/compare/v0.2.0-4...v0.2.0-5) (2021-08-06)

## [0.2.0-4](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/compare/v0.2.0-3...v0.2.0-4) (2021-08-06)


### Bug Fixes

* **ci-configuration:** fix erroneous before_script ([40f9595](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/40f9595f2dc48af08408cf3e2dbeb90cfbcf35ba))

## [0.2.0-3](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/compare/v0.2.0-1...v0.2.0-3) (2021-08-06)


### Bug Fixes

* **ci-configuration:** fix wrong needs-dependency ([1142c5d](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/1142c5dc2fbab60efdf008fdc8c59ccd485d7403))

## 0.2.0-1 (2021-08-06)


### Features

* **redis:** add redis package to mono-repo ([bd81859](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/bd81859f75198e5c6934e3cd616a014ff6b389ed)), closes [#42](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/issues/42)


### Bug Fixes

* **.gitlab-ci.yml:** fix invalid ci definition ([8c6ef16](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/8c6ef1637d90416529fc29a0410ce36961cd05b7))
* **ci-configuration:** fix invalid dependency ([3f6204c](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/3f6204ce5d5cafe090c1a1b578c0281615ea9caf))
* **conversion-service:** add return statement in controller ([1716776](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/171677603a0450a973fa278d91a8e1d7d32035ba))
* **conversion-service:** fix dangling comma in package.json ([bfd621b](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/bfd621bac560f4e8fd08e93be2a12e83a5050812))
* **package.json:** add missing script for tests with coverage ([bad51b5](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/bad51b51fdd1d48be965723365942a80edd757ae))


### Code Refactoring

* **auto-scaler:** rename variables, remove unnecessary comment ([2dfa96f](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/2dfa96fad900dafb366b708ebf401691e93eb676))
* **conversion-service:** remove unused code(-comments) ([f1570b7](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/f1570b7b824f7c9e15a0d141165c9c2d02bb56b4))
* **eslint-configuration:** fix eslint configuration ([d6cc853](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/d6cc8533f2ada6313b83896da64f149a54368653)), closes [#26](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/issues/26)


### Docs

* **auto-scaler:** add preliminary documentation ([5feab0c](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/5feab0cb930bcbb890394aee15abb6c1a675694b)), closes [#15](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/issues/15)
* **auto-scaler:** add syntax-highlighting in docs ([9b67ef1](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/9b67ef18cff5eb3c9f0b4c67ccf56693c7ccae66))
* **readme:** add project readme ([b3a8faa](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/b3a8faa5de7a859b07f4979fd87344c1eb9d9ed2))


### Styling

* **auto-scaler:** double-quotes instead of single-quotes ([0cd7b18](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/0cd7b180a5e63cf27393f13005ea0f968e73ca06))
* **auto-scaler:** fix destructing order and formatting ([8ccdd4e](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/8ccdd4e3ed9dcabbd91ec580f4493794dcd29cc8))
* **auto-scaler:** fix eslint violations ([ddcd8ba](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/ddcd8bab68ab7a0f3b82713069ac40dc7d183fc7))
* **auto-scaler:** fix eslint violations, fix review suggestions ([e9cba63](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/e9cba63865fa3293e26ecf501b6bba791c90b89c))
* **redis-tests:** add empty export ([04a9b39](https://gitlab.internal.team-parallax.com/belwue/dynamic-conversion-service/commit/04a9b39e10b9970694a220f825afbeca950bf8db))
