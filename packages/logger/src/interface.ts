import { ELogLevel } from "./enum"
export interface ILoggerOptions {
	logLevel?: ELogLevel,
	serviceName?: string
}